<?php

namespace App\Model\Api;



class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/search.json';
    const ENDPOINT_ENTRY = '/info.json?id=t3_8w9hrp';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function makeSearch($data)
    {
        $word = $data['word'];
        $sort = $data['sort'];
        $limit = $data['limit'];
        return $this->makeQuery(
            self::ENDPOINT_PING,
            self::METHOD_GET,
            ['q' => "{$word}", 'sort' => "{$sort}", 'limit' => "{$limit}", 'type' => 'link']);
    }

    public function inEntry(string $data)
    {
        try {
            $this->curl->get('https://www.reddit.com/api/info.json', ['id' => "{$data}"]);
            return $this->curl->response;
        } catch (\ErrorException $e) {
        }
    }
}

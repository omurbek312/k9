<?php

namespace App\Model\User;

use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    private $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data)
    {
        $admin = new User();
        $admin->setUsername($data['username']);
        $password = md5($data['password']) . md5($data['password'] . '2');
        $admin->setPassword($password);

        return $admin;
    }

    /**
     * @param User $user
     */
    public function makeSessionUser(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}

<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterForListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('word', TextType::class)
            ->add('sort', ChoiceType::class,
                ['choices' => [
                    'по релевантности' => 'relevance',
                    'горячие' => 'hot',
                    'в топе' => 'top',
                    'новые' => 'new',
                    'коментируемые' => 'comments',
                ]])->add('limit', ChoiceType::class, ['choices' => [
                '8шт' => 8,
                '12шт' => 12,
                '16шт' => 16,
                '50шт' => 50,
                '100шт' => 100,
            ]])
            ->add('find', SubmitType::class,['label'=>'Искать']);
    }
}
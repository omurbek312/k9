<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class UserFavariteEntry
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserFavariteEntryRepository")
 */
class UserFavariteEntry

{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $entryId;
    /**
     * @var mixed
     * @ORM\ManyToOne(targetEntity="App\Entity\User",inversedBy="entry")
     * @ORM\Column(type="string")
     */
    protected $user;

    /**
     * @param mixed $id
     * @return UserFavariteEntry
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @param mixed $entryId
     * @return UserFavariteEntry
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntryId()
    {
        return $this->entryId;
    }
}
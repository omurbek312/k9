<?php

namespace App\DataFixtures;


use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $adminHandler;

    public function __construct(UserHandler $AdminHandler)
    {
        $this->adminHandler = $AdminHandler;
    }

    public function load(ObjectManager $manager)
    {

        $admin = $this->adminHandler->createNewUser([
            'username' => 'admin',
            'password' => 'pass_1234',
        ]);

        $manager->persist($admin);
        $manager->flush();
    }
}

<?php

namespace App\Controller;

use App\Entity\UserFavariteEntry;
use App\Form\FilterForListType;
use App\Model\Api\ApiContext;
use Curl\Curl;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class  indexController extends Controller
{
    /**
     * @Route("/",name="home")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function homeAction(Request $request, ApiContext $apiContext)
    {
        $form = $this->createForm(FilterForListType::class);
        $form->handleRequest($request);
        $data = $form->getData();
        $dataApi = $apiContext->makeSearch($data);
        $sort = $data['sort'];
        $dataReddits = $dataApi['data']['children'];
        $allData = [];
        foreach ($dataReddits as $key => $dataReddit) {
            $allData[] = ['image' => $dataReddits[$key]['data']['thumbnail'],
                'title' => $dataReddits[$key]['data']['title'],
                'author' => $dataReddits[$key]['data']['author'],
                'date' => $dataReddits[$key]['data']['created_utc'],
                'id' => $dataReddits[$key]['data']['name']
            ];
        };
        return $this->render('home.html.twig',
            [
                'form' => $form->createView(),
                'data' => $allData,
                'sort' => $sort
            ]);
    }

    /**
     * @Route("/inEntry/{id}",name="entry")
     * @param string $id
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function entry(string $id, ApiContext $apiContext)
    {
        $entry = $apiContext->inEntry($id);
        $dataReddits = $entry['data']['children'][0]['data'];
        $allData [] = ['title' => $dataReddits['title'],
            'author' => $dataReddits['author'],
            'date' => $dataReddits['created_utc'],
            'image' => $dataReddits['thumbnail']];

        return $this->render('inEntry.html.twig', ['entry' => $allData]);
    }

    /**
     * @Route("/inMyFavorites/{id}",name="myFavorites")
     * @param string $id
     * @param ObjectManager $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function AddEntryInMyFavorites(string $id, ObjectManager $em)
    {
        $entry = new UserFavariteEntry();
        $user = $this->getUser();
        $entry->setUser($user);
        $entry->setEntryId($id);
        $em->persist($entry);
        $em->flush();
        foreach ($user->getEntryId() as $entryes) {

        }

        return $this->render('MyFavorites.html.twig', ['user' => $user]);
    }
}
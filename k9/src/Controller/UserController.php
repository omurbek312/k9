<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationForm\AuthorizationType;
use App\Form\RegistrationForm\RegistrationType;
use App\Model\User\UserHandler;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class  UserController extends Controller
{

    /**
     * @Route("/registration",name="registration")
     * @param Request $request
     * @param ObjectManager $em
     * @return Response
     */
    public function registrationAction(Request $request, ObjectManager $em)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        $data = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {
            $password = md5($user->getPassword()) . md5($user->getPassword() . '2');
            $user->setPassword($password);
            $em->persist($user);
            $em->flush();
        }
        return $this->render('registration.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/log-in",name="log-in")
     * @param Request $request
     * @param UserHandler $user
     * @param ObjectManager $em
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginForAdmin(Request $request, UserHandler $user, ObjectManager $em)
    {

        $form = $this->createForm(AuthorizationType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $password = $data['password'];
            $userName = $data['username'];

            $findUser = $em->getRepository('App:User')->checkUser($userName, $password);
            if ($findUser !== null) {
                $user->makeSessionUser($findUser);
                return $this->redirectToRoute('home');

            } else {

                return new Response('Вы ввели не верные данные');
            }
        }
        return $this->render('userAuthorization.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/logout")
     */
    public function logout()
    {
        return $this->redirectToRoute('home');
    }
}